using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterHealthbar : MonoBehaviour
{
    [SerializeField] private Color m_FullHealthColor = Color.green;       // The color the health bar will be when on full health.
    [SerializeField] private Color m_ZeroHealthColor = Color.red;
    [SerializeField] private Image m_fillImage;

    public Image fillImage => this.m_fillImage;

    public void SetValue(float value)
    {
        this.fillImage.fillAmount = value;
        this.fillImage.color = Color.Lerp (m_ZeroHealthColor, m_FullHealthColor, value);
    }
}
