using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using UnityEngine;

namespace TankTestTask.Gameplay
{
    public class CameraSystem : MonoBehaviour
    {
        [SerializeField] private CinemachineVirtualCamera m_virtualCamera;

        public CinemachineVirtualCamera virtualCamera => this.m_virtualCamera;
    }
}
