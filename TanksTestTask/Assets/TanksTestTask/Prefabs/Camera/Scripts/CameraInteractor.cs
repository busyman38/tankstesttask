using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TankTestTask.Gameplay
{
    public class CameraInteractor : Interactor
    {
        #region CONST

        protected const string CAMERA_PREFAB_NAME = "[CAMERA]";

        #endregion

        public CameraSystem cameraSystem { get; private set; }

        public override void OnCreate()
        {
            var cameraPrefab = Resources.Load<CameraSystem>(CAMERA_PREFAB_NAME);
            this.cameraSystem = GameObject.Instantiate(cameraPrefab);
        }
    }
}

