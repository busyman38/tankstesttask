using TanksTestTask.Gameplay;
using UnityEngine;

namespace TankTestTask.Gameplay
{
    public class EnemyShooter : Enemy
    {
        [SerializeField] private EnemyProjectile m_projectile;
        [SerializeField] private float m_launchProjectileForce;

        public EnemyProjectile projectile => this.m_projectile;
        public float launchProjectileForce => this.m_launchProjectileForce;

        protected override void InitBehaviors()
        {
            base.InitBehaviors();
            this.AddBehavior<EnemyBehaviourShootAttack>();
        }
    }
}

