﻿using System;
using System.Collections;
using UnityEngine;

namespace TankTestTask.Gameplay
{
    public class EnemyAIManipulator : CharacterManipulator
    {
        [SerializeField] private float attackDelay = 1f;
        
        private Enemy m_enemyCharacter;
        private MainCharacterInteractor mainCharacterInteractor;
        private bool isAttacking;

        public Enemy enemyCharacter => m_enemyCharacter;

        #region MONO

        private void Start()
        {
            this.m_enemyCharacter = (Enemy) this.character;
            this.mainCharacterInteractor = Game.GetInteractor<MainCharacterInteractor>();
        }

        private void OnTriggerStay(Collider other)
        {
            if (other.gameObject.layer != LayerMask.NameToLayer("Players") || isAttacking) return;
            
            this.SetAttackBehaviour();
            StartCoroutine(AttackRoutine());
        }

        private void OnTriggerExit(Collider other)
        {
            if (isAttacking)
            {
                StopCoroutine(AttackRoutine());
            }
            
            this.enemyCharacter.SetBehavior<EnemyBehaviourChaseEenemy>();
        }

        #endregion

        protected virtual void SetAttackBehaviour()
        {
            this.enemyCharacter.SetBehavior<EnemyBehaviourAttack>();
        }

        private IEnumerator AttackRoutine()
        {
            this.isAttacking = true;
            this.m_enemyCharacter.Attack();
            
            yield return new WaitForSeconds(this.attackDelay);
            
            this.isAttacking = false;
        }
    }
}