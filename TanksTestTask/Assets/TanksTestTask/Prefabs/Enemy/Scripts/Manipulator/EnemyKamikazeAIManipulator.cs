﻿using System;
using UnityEngine;

namespace TankTestTask.Gameplay
{
    public class EnemyKamikazeAIManipulator : CharacterManipulator
    {
        private Enemy enemyCharacter;
        private MainCharacterInteractor mainCharacterInteractor;

        #region MONO

        private void Start()
        {
            this.enemyCharacter = (Enemy) this.character;
            this.mainCharacterInteractor = Game.GetInteractor<MainCharacterInteractor>();
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.layer != LayerMask.NameToLayer("Players")) return;
            
            var player = mainCharacterInteractor.mainCharacter;
            player.Hit(this, this.enemyCharacter.currentStats.damage);
            this.enemyCharacter.Die();
        }

        #endregion
    }
}