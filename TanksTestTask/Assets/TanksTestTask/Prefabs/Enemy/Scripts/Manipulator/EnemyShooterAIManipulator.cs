﻿using System;
using System.Collections;
using UnityEngine;

namespace TankTestTask.Gameplay
{
    public class EnemyShooterAIManipulator : EnemyAIManipulator
    {
        protected override void SetAttackBehaviour()
        {
            this.enemyCharacter.SetBehavior<EnemyBehaviourShootAttack>();
        }
    }
}