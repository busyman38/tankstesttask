using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace TankTestTask.Gameplay
{
    public class Enemy : Character
    {
        #region EVENTS

        public event Action<Enemy> OnDieEvent;

        #endregion
        
        [SerializeField] private CharacterStats m_defaultStats;
        [SerializeField] private NavMeshAgent m_navMeshAgent;
        [SerializeField] private CharacterHealthbar m_uiHealthBar;

        public override CharacterStats currentStats => m_defaultStats;
        private CharacterStats defaultStats;
        public NavMeshAgent navMeshAgent => this.m_navMeshAgent;

        #region MONO

        private void Start()
        {
            this.defaultStats = this.currentStats.Clone();
            this.m_uiHealthBar.SetValue(this.currentStats.health / this.defaultStats.health);
            this.SetBehavior<EnemyBehaviourChaseEenemy>();
        }

        #endregion
        
        #region BEHAVIOR

        protected override void InitBehaviors()
        {
            this.behaviorsMap = new Dictionary<Type, CharacterBehaviour>();
            this.AddBehavior<EnemyBehaviourChaseEenemy>();
        }

        #endregion

        public override void Attack()
        {
            if (!this.behavior.CanAttack())
                return;

            this.behavior.Attack();
        }

        public override void Die()
        {
            this.OnDieEvent?.Invoke(this);
            Destroy(this.gameObject);
        }

        public override void Hit(object sender, int damage)
        {
            this.currentStats.health -= (int) Mathf.Round(damage * this.currentStats.armor);

            var value = (float)this.currentStats.health / this.defaultStats.health;
            this.m_uiHealthBar.SetValue(value);

            if (this.currentStats.health <= 0) this.Die();
        }

        public override void Move(object sender, float inputDirection)
        {
            throw new System.NotImplementedException();
        }

        public override void Move(object sender, float inputDirection, float turnValue)
        {
            throw new System.NotImplementedException();
        }
    }
}

