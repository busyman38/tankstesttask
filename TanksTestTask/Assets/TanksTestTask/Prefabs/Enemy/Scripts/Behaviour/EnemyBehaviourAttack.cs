﻿using System;
using UnityEngine;

namespace TankTestTask.Gameplay
{
    public class EnemyBehaviourAttack : CharacterBehaviour
    {
        private Enemy m_enemyCharacter;
        private CharacterStats stats;
        private Rigidbody rb;
        private MainCharacterInteractor mainCharInteractor;
        private MainCharacter m_player;

        protected Enemy enemyCharacter => this.m_enemyCharacter;
        protected MainCharacter player => this.m_player;
        
        #region MONO

        private void Awake()
        {
            this.m_enemyCharacter = this.GetComponent<Enemy>();
            this.stats = this.m_enemyCharacter.currentStats;
            this.rb = this.m_enemyCharacter.rb;
            this.mainCharInteractor = Game.GetInteractor<MainCharacterInteractor>();
            this.m_player = this.mainCharInteractor.mainCharacter;
        }

        private void Update()
        {
            if (!m_player) return;

            this.m_enemyCharacter.transform.LookAt(m_player.transform);
        }

        #endregion
        
        public override void Attack()
        {
            if (!this.m_player) return;

            this.m_player.Hit(m_enemyCharacter, m_enemyCharacter.currentStats.damage);
        }
    }
}