using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace TankTestTask.Gameplay
{
    public class EnemyBehaviourChaseEenemy : CharacterBehaviour
    {
        private Enemy enemyCharacter;
        private CharacterStats stats;
        private NavMeshAgent navMeshAgent;
        private MainCharacterInteractor mainCharacterInteractor;

        #region MONO

        private void Awake()
        {
            this.enemyCharacter = this.GetComponent<Enemy>();
            this.navMeshAgent = this.enemyCharacter.navMeshAgent;
            this.navMeshAgent.speed = this.enemyCharacter.currentStats.speed;
        }

        #endregion

        protected override void OnEnter()
        {
            this.mainCharacterInteractor = Game.GetInteractor<MainCharacterInteractor>();
        }

        protected override void OnExit()
        {
            this.navMeshAgent.isStopped = true;
            this.navMeshAgent.ResetPath();
        }

        private void FixedUpdate()
        {
            if (!Game.isInitialized || !this.mainCharacterInteractor.mainCharacter) return;

            var playerPosition = this.mainCharacterInteractor.mainCharacter.transform.position;
            this.navMeshAgent.SetDestination(playerPosition);
        }
    }
}

