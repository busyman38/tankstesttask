﻿using System;
using UnityEngine;

namespace TankTestTask.Gameplay
{
    public class EnemyBehaviourShootAttack : EnemyBehaviourAttack
    {
        public override void Attack()
        {
            var enemyShooter = (EnemyShooter) this.enemyCharacter;
            var firePoints = enemyShooter.skin.firePoints;
            
            foreach (var firePoint in firePoints)
            {
                var projectileInstance = Instantiate(enemyShooter.projectile, firePoint.position, firePoint.rotation);
                projectileInstance.Setup(enemyShooter.currentStats.damage);
                projectileInstance.rb.velocity = enemyShooter.launchProjectileForce * firePoint.forward;
            }
        }
    }
}