﻿namespace TankTestTask.Gameplay
{
    public class EnemyFighter : Enemy
    {
        protected override void InitBehaviors()
        {
            base.InitBehaviors();
            this.AddBehavior<EnemyBehaviourAttack>();
        }
    }
}