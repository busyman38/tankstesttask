using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace TankTestTask.Gameplay
{
    public class EnemiesSpawnInteractor : Interactor
    {
        #region CONST

        protected const string ENEMIES_PREFABS_PATH = "EnemiesGO";

        #endregion

        private int maxEnemiesCount = 10;
        private List<SpawnZone> spawnZonesMap;
        private List<Enemy> spawnedEnemies;
        private List<Enemy> loadedEnemies;

        public override void OnCreate()
        {
            base.OnCreate();
            this.loadedEnemies = Resources.LoadAll<Enemy>(ENEMIES_PREFABS_PATH).ToList();
            this.spawnedEnemies = new List<Enemy>();
            this.spawnZonesMap = new List<SpawnZone>();
        }

        public override void OnUpdate()
        {
            if (spawnedEnemies.Count < this.maxEnemiesCount)
            {
                this.SpawnEnemy();
            }
        }

        #region SPAWN

        private void SpawnEnemy()
        {
            var randomEnemyIndex = this.GetRandomIndex(this.loadedEnemies.Count());
            var enemyPrefab = this.loadedEnemies[randomEnemyIndex];
            var enemyTransform = enemyPrefab.transform;
            var randomSpawnZoneIndex = this.GetRandomIndex(this.spawnZonesMap.Count());
            var randomSpawnZone = this.spawnZonesMap[randomSpawnZoneIndex];
            var randomPosition = randomSpawnZone.GetRandomPosition();
            randomPosition.y = enemyTransform.position.y;
            
            var enemyInstance = Object.Instantiate(enemyPrefab, randomPosition, enemyTransform.rotation);
            enemyInstance.OnDieEvent += OnEnemyDie;
            this.spawnedEnemies.Add(enemyInstance);
        }

        public void RegisterSpawnZone(SpawnZone zone)
        {
            this.spawnZonesMap.Add(zone);
        }

        private int GetRandomIndex(int itemsCount)
        {
            return Random.Range(0, itemsCount);
        }

        #endregion
        
        #region CALLBACKS

        private void OnEnemyDie(Enemy enemy)
        {
            this.spawnedEnemies.Remove(enemy);
        }

        #endregion
    }
}

