namespace TankTestTask.Gameplay {
    public interface IInteractor
    {
        void Start();
        void OnCreate();
        void OnDestroy();
        void OnUpdate();
    }
}
