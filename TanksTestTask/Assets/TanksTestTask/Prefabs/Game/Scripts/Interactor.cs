using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TankTestTask.Gameplay
{
    public abstract class Interactor : IInteractor
    {
        public Interactor()
        {
            this.OnCreate();
        }

        public virtual void OnCreate()
        {
            Game.OnGameUpdateEvent += OnUpdate;
        }

        public virtual void OnDestroy()
        {
            Game.OnGameUpdateEvent += OnUpdate;
        }

        public virtual void Start() { }
        public virtual void OnUpdate() { }
    }
}

