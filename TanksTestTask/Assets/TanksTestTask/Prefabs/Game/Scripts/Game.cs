using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace TankTestTask.Gameplay
{
    public class Game : MonoBehaviour
    {
        public enum State
        {
            Initializing,
            Initialized
        }

        #region EVENTS

        public static event Action OnGameInitializedEvent;
        public static event Action OnGameUpdateEvent;

        #endregion

        protected static Game instance;
        private Dictionary<Type, IInteractor> m_interactorsMap;

        public static State state { get; private set; }
        public static bool isInitialized => state == State.Initialized;

        #region MONO

        private void Awake()
        {
            if (instance != null) return;

            instance = this;

            this.Initialize();
        }

        private void Update()
        {
            if (!isInitialized) return;

            OnGameUpdateEvent?.Invoke();
        }

        #endregion

        #region INITIALIZING

        private void Initialize()
        {
            state = State.Initializing;

            this.CreateAllInteractors();
            this.StartAllInteractors();

            state = State.Initialized;
            OnGameInitializedEvent?.Invoke();
        }

        #endregion

        #region INTERACTORS

        private void CreateAllInteractors()
        {
            m_interactorsMap = new Dictionary<Type, IInteractor>();

            m_interactorsMap.Add(typeof(WorldInteractor), new WorldInteractor());
            m_interactorsMap.Add(typeof(CameraInteractor), new CameraInteractor());
            m_interactorsMap.Add(typeof(MainCharacterWeaponInteractor), new MainCharacterWeaponInteractor());
            m_interactorsMap.Add(typeof(MainCharacterInteractor), new MainCharacterInteractor());
            m_interactorsMap.Add(typeof(EnemiesSpawnInteractor), new EnemiesSpawnInteractor());
        }

        private void StartAllInteractors()
        {
            foreach (var interactor in m_interactorsMap)
            {
                interactor.Value.Start();
            }
        }

        public static T GetInteractor<T>() where T : IInteractor
        {
            var type = typeof(T);
            var founded = instance.m_interactorsMap.TryGetValue(type, out IInteractor resultInteractor);

            if (founded)
                return (T)resultInteractor;

            foreach (var interactor in instance.m_interactorsMap.Values.OfType<T>())
            {
                return interactor;
            }

            throw new KeyNotFoundException($"Key: {type}");
        }

        #endregion

    }
}