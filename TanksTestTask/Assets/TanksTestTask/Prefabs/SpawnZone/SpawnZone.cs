using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

namespace TankTestTask.Gameplay
{
    public class SpawnZone : MonoBehaviour
    {
        [SerializeField] private MeshFilter meshFilter;

        #region MONO

        private void Awake()
                {
                    if(Game.isInitialized)
                        this.RegisterInEnemiesSpawnInteractor();
                    else
                    {
                        Game.OnGameInitializedEvent += OnGameInitialized;
                    }
                }

        #endregion
        
        public Vector3 GetRandomPosition()
        {
            var mesh = this.meshFilter.mesh;
    
            var minx = mesh.bounds.min.x;
            var minz = mesh.bounds.min.z;
            var maxx = mesh.bounds.max.x;
            var maxz = mesh.bounds.max.z;
    
            var randX = Random.Range(minx, maxx);
            var randZ = Random.Range(minz, maxz);
            var localPosition = new Vector3(randX, 0, randZ);
            return transform.TransformPoint(localPosition);
        }

        private void RegisterInEnemiesSpawnInteractor()
        {
            var enemiesSpawnInteractor = Game.GetInteractor<EnemiesSpawnInteractor>();
            enemiesSpawnInteractor.RegisterSpawnZone(this);
        }

        #region CALLBACKS

        private void OnGameInitialized()
        {
            Game.OnGameInitializedEvent -= OnGameInitialized;
            this.RegisterInEnemiesSpawnInteractor();
        }

        #endregion
    }
}

