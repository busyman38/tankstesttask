using UnityEngine;

namespace TankTestTask.Gameplay
{
    public class CharacterSkin : MonoBehaviour
    {
        [SerializeField] private Transform[] m_firePoints;
        
        public Transform[] firePoints => m_firePoints;
    }
}