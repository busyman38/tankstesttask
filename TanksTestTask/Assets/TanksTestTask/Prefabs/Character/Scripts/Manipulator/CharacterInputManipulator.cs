using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

namespace TankTestTask.Gameplay
{
    public class CharacterInputManipulator : CharacterManipulator
    {
        private InputManager inputManager;
        private InputControl input;
        private MainCharacter mainCharacter;

        #region MONO

        private void Start()
        {
            this.mainCharacter = (MainCharacter) this.character;

            this.inputManager = InputManager.instance;
            this.input = this.inputManager.inputGameplay;
            this.input.Enable();

            this.input.Gameplay.Attack.performed += this.OnAttackPerformed;
            this.input.Gameplay.ChangeWeaponNext.performed += this.OnNextWeaponPerformed;
            this.input.Gameplay.ChangeWeaponPrev.performed += this.OnPrevWeaponPerformed;
        }

        public void OnDestroy()
        {
            this.input.Disable();

            this.input.Gameplay.Attack.performed -= this.OnAttackPerformed;
        }

        private void FixedUpdate()
        {
            if (this.inputManager.isGameplayInputLocked)
                return;

            var inputDirection = this.input.Gameplay.Move.ReadValue<float>();
            var inputRotation = this.input.Gameplay.Turn.ReadValue<float>();

            this.Move(inputDirection, inputRotation);
        }

        #endregion

        private void EquipNextWeapon()
        {
            this.mainCharacter.EquipNextWeapon();
        }

        private void EquipPrevWeapon()
        {
            this.mainCharacter.EquipPrevWeapon();
        }
        
        #region CALLBACKS

        private void OnAttackPerformed(InputAction.CallbackContext obj)
        {
            this.Attack();
        }

        private void OnPrevWeaponPerformed(InputAction.CallbackContext obj)
        {
            this.EquipPrevWeapon();
        }

        private void OnNextWeaponPerformed(InputAction.CallbackContext obj)
        {
            this.EquipNextWeapon();
        }

        #endregion
    }
}