using System;
using UnityEngine;

namespace TankTestTask.Gameplay
{
    public class CharacterManipulator : MonoBehaviour
    {
		public IControllableCharacter character { get; private set; }

		#region MONO

		private void Awake()
		{
			this.character = this.GetComponentInChildren<IControllableCharacter>();
			if (this.character == null)
				throw new Exception($"There is no IControllableCharacter interfaces on the object: {this.gameObject.name}");

			this.OnAwake();
		}

		protected virtual void OnAwake() { }

		#endregion

		protected virtual void Move(float inputDirection, float turnDirection)
		{
			this.character.Move(this, inputDirection, turnDirection);
		}

		protected virtual void Move(float inputDirection)
		{
			this.character.Move(this, inputDirection);
		}

		protected virtual void Attack()
        {
			this.character.Attack();
        }
	}
}

