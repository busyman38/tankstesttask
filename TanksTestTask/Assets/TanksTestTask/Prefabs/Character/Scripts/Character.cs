using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TankTestTask.Gameplay
{
    public abstract class Character : MonoBehaviour, IControllableCharacter
    {
        [SerializeField] private Rigidbody m_rb;
        [SerializeField] private CharacterSkin m_skin;
        [SerializeField] private CharacterMetaData m_meta;

        public Rigidbody rb => this.m_rb;
        public abstract CharacterStats currentStats { get;}
        public CharacterSkin skin
        {
            get => this.m_skin;
            private set => this.m_skin = value;
        }
        public CharacterMetaData meta => this.m_meta;

		protected Dictionary<Type, CharacterBehaviour> behaviorsMap = new Dictionary<Type, CharacterBehaviour>();
		protected CharacterBehaviour behavior;

		#region MONO

		protected virtual void Awake()
        {
            this.InitBehaviors();
        }

		#endregion

		#region BEHAVIOR

		protected virtual void InitBehaviors() { }

		protected void AddBehavior<T>() where T : CharacterBehaviour
		{
			var type = typeof(T);
			var createdBehavior = this.gameObject.AddComponent<T>();
			createdBehavior.enabled = false;
			this.behaviorsMap[type] = createdBehavior;
		}

		public bool SetBehavior<T>() where T : CharacterBehaviour
		{
			if (this.behavior is T)
				return false;

			var nextBehaviorExists = this.behaviorsMap.TryGetValue(typeof(T), out var nextBehavior);
			if (!nextBehaviorExists)
				return false;

			if (this.behavior != null)
				this.behavior.Exit();
			this.behavior = nextBehavior;
			this.behavior.Enter();

			return true;
		}

        #endregion

        #region ICONTROLLABLE METHODS

        public abstract void Attack();
        public abstract void Move(object sender, float inputDirection);
        public abstract void Move(object sender, float inputDirection, float turnValue);

        #endregion

        public abstract void Hit(object sender, int damage);
        public abstract void Die();
    }
}