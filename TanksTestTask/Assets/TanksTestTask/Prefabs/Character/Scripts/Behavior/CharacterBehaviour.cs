using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TankTestTask.Gameplay
{
    public class CharacterBehaviour : BehaviourBase
    {
        public virtual void Attack() { }
        public virtual void Move(object sender, float inputDirection) { }
        public virtual void Move(object sender, float inputDirection, float turnDirection) { }
        public virtual void Hit(object sender, int damage) { }
        public virtual void Die() { }

        public virtual bool CanAttack()
        {
            return true;
        }

        public virtual bool CanMove()
        {
            return true;
        }
    }
}

