using System;

namespace TankTestTask.Gameplay
{
	public interface IBehaviour
	{

		#region EVENTS

		event Action OnBehaviourEnteredEvent;
		event Action OnBehaviourExitedEvent;

		#endregion

		void Enter();
		void Exit();

		bool CanExit();
	}
}