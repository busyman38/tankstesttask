using System;
using UnityEngine;

namespace TankTestTask.Gameplay
{
	public abstract class BehaviourBase : MonoBehaviour, IBehaviour
	{

		#region EVENTS

        public event Action OnBehaviourEnteredEvent;
        public event Action OnBehaviourExitedEvent;

        #endregion


        public void Enter()
		{
			this.enabled = true;
			this.OnEnter();
			this.OnBehaviourEnteredEvent?.Invoke();
		}

		protected virtual void OnEnter() { }

		public void Exit()
		{
			this.enabled = false;
			this.OnExit();
			this.OnBehaviourExitedEvent?.Invoke();
		}

		public virtual bool CanExit()
		{
			return true;
		}

		protected virtual void OnExit() { }
	}
}