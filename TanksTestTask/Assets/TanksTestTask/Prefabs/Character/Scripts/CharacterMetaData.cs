using System;

namespace TankTestTask.Gameplay
{
	[Serializable]
	public sealed class CharacterMetaData
	{
		public string id;
		public string name;
		public string description;
	}
}