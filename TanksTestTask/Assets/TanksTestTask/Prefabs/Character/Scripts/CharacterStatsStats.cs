using System;

namespace TankTestTask.Gameplay
{
    [Serializable]
    public class CharacterStats
    {
        public int health;
        public float armor;
        public float speed;
        public int damage;

        public CharacterStats Clone()
        {
            return new CharacterStats
            {
                health = this.health,
                armor = this.armor,
                damage = this.damage,
                speed = this.speed,
            };
        }
    }
}

    

