using UnityEngine;

namespace TankTestTask.Gameplay {

    public interface IControllableCharacter {
        void Move(object sender, float inputDirection);
        void Move(object sender, float inputDirection, float turnValue);
        void Attack();
    }
}
