using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TankTestTask.Gameplay
{
    public class CharacterBehaviourIdle : CharacterBehaviour
    {
		private Character character;

        #region MONO

        private void Awake()
        {
            this.character = this.GetComponent<Character>();
        }

        #endregion

        public override void Attack()
        {
            this.character.SetBehavior<CharacterBehaviourMoveAndAttack>();
            this.character.Attack();
        }

        public override void Move(object sender, float inputDirection)
		{
			if (inputDirection == 0f)
				return;

			this.character.SetBehavior<CharacterBehaviourMoveAndAttack>();
		}

        public override void Move(object sender, float inputDirection, float turnRotation)
        {
            if (inputDirection == 0f && turnRotation == 0f)
                return;

            this.character.SetBehavior<CharacterBehaviourMoveAndAttack>();
        }
    }
}

