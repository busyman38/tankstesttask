using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TankTestTask.Gameplay
{
    public class CharacterBehaviourAttack : CharacterBehaviour
    {
        private MainCharacter character;
        private MainCharacterStats stats;

        #region MONO

        private void Awake()
        {
            this.character = this.GetComponent<MainCharacter>();
            this.stats = (MainCharacterStats) this.character.currentStats;
        }

        #endregion

        public override void Move(object sender, float inputDirection)
        {
            if (inputDirection == 0f)
                return;

            this.character.SetBehavior<CharacterBehaviourMoveAndAttack>();
        }

        public override void Move(object sender, float inputDirection, float turnRotation)
        {
            if (inputDirection == 0f && turnRotation == 0f)
                return;

            this.character.SetBehavior<CharacterBehaviourMoveAndAttack>();
        }

        public override void Attack()
        {
            //var firePoint = this.character.firePoint;
            /*var projectileInstance = Instantiate(character.equipedWeapon.projectileGO, firePoint.position, firePoint.rotation);
            projectileInstance.Setup(this.stats.damage);
            projectileInstance.rb.velocity = this.stats.launchProjectileForce * firePoint.forward;*/
        }
    }
}

