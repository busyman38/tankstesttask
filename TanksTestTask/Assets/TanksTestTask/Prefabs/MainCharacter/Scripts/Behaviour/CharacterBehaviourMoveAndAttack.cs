using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TankTestTask.Gameplay
{
	public class CharacterBehaviourMoveAndAttack : CharacterBehaviour
	{
		private MainCharacter character;
		private MainCharacterStats stats;
		private Rigidbody rb;

		#region MONO

		private void Awake()
		{
			this.character = this.GetComponent<MainCharacter>();
			this.stats = (MainCharacterStats) this.character.currentStats;
			this.rb = this.character.rb;
		}

        #endregion

        public override void Attack()
        {
	        var firePoints = this.character.currentSkin.firePoints;
	        
	        foreach (var firePoint in firePoints)
	        {
		        var projectileInstance = Instantiate(character.equipedWeapon.projectileGO, firePoint.position, firePoint.rotation);
		        projectileInstance.Setup(this.stats.damage);
		        projectileInstance.rb.velocity = this.stats.launchProjectileForce * firePoint.forward;
	        }
        }

        public override void Move(object sender, float inputDirection)
		{
			if (inputDirection == 0f)
			{
				this.character.SetBehavior<CharacterBehaviourIdle>();
				return;
			}

			var movement = inputDirection * this.stats.speed * Time.deltaTime * character.transform.forward;
			this.rb.MovePosition(this.rb.position + movement);
		}

        public override void Move(object sender, float inputDirection, float turnDirection)
        {
			if (inputDirection == 0f && turnDirection == 0f)
			{
				this.character.SetBehavior<CharacterBehaviourIdle>();
				return;
			}

			this.Move(sender, inputDirection);

			if (!(this.stats is MainCharacterStats mainCharStats)) return;
			
			var turn = turnDirection * mainCharStats.turnSpeed * Time.deltaTime;
			var turnRotation = Quaternion.Euler(0f, turn, 0f);
			this.rb.MoveRotation(this.rb.rotation * turnRotation);
        }
    }
}

