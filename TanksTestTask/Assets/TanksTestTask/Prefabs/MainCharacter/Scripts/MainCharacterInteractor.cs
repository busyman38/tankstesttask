using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TankTestTask.Gameplay
{
    public class MainCharacterInteractor : Interactor
    {
        #region CONST

        protected const string MAIN_CHAR_PREFAB_PATH = "MainCharacterGO";

        #endregion

        private WorldInteractor worldInteractor;
        private MainCharacter m_mainCharacter;

        public MainCharacter mainCharacter => this.m_mainCharacter;

        public override void Start()
        {
            this.worldInteractor = Game.GetInteractor<WorldInteractor>();

            if (this.worldInteractor.world != null)
            {
                this.CreatePlayer();
                this.BindPlayerToCam();
            }
            else
            {
                worldInteractor.OnWorldRegisteredEvent += this.OnWorldRegistered;
            }
                
        }

        private void CreatePlayer()
        {
            var mainCharacterPrefab = Resources.Load<MainCharacter>(MAIN_CHAR_PREFAB_PATH);
            var playerSpawnPoint = worldInteractor.world.playerSpawnPoint;

            this.m_mainCharacter = GameObject.Instantiate<MainCharacter>(mainCharacterPrefab);
            this.m_mainCharacter.gameObject.transform.position = playerSpawnPoint.position;
        }

        private void BindPlayerToCam()
        {
            var cameraInteractor = Game.GetInteractor<CameraInteractor>();

            if (cameraInteractor.cameraSystem == null) throw new Exception("Camera is not loaded");

            cameraInteractor.cameraSystem.virtualCamera.Follow = this.mainCharacter.transform;
        }

        #region CALLBACKS

        private void OnWorldRegistered()
        {
            this.worldInteractor.OnWorldRegisteredEvent -= this.OnWorldRegistered;

            this.CreatePlayer();
            this.BindPlayerToCam();
        }

        #endregion       
    }
}

