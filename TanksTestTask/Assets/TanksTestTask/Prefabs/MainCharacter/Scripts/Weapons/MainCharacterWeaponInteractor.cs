using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace TankTestTask.Gameplay
{
    public class MainCharacterWeaponInteractor : Interactor
    {
        #region CONST

        protected const string WEAPONS_INFO_PATH = "Weapons/Info";

        #endregion

        private List<MainCharacterWeaponInfo> weaponsInfoMap;

        public override void OnCreate()
        {
            base.OnCreate();

            this.weaponsInfoMap = new List<MainCharacterWeaponInfo>();
            this.weaponsInfoMap = Resources.LoadAll<MainCharacterWeaponInfo>(WEAPONS_INFO_PATH).ToList();
        }

        public MainCharacterWeaponInfo GetDefaultWeapon()
        {
            return this.weaponsInfoMap.Where(info => info.isActiveByDefault).First();
        }

        public bool HasNextWeapon(MainCharacterWeaponInfo currentWeapon)
        {
            var lastWeapon = this.weaponsInfoMap.Last();
            return (currentWeapon != lastWeapon);
        }

        public bool HasPrevWeapon(MainCharacterWeaponInfo currentWeapon)
        {
            var lastWeapon = this.weaponsInfoMap.First();
            return (currentWeapon != lastWeapon);
        }

        public MainCharacterWeaponInfo GetNextWeapon(MainCharacterWeaponInfo currentWeapon)
        {
            var currentIndex = this.weaponsInfoMap.IndexOf(currentWeapon);

            if (!this.HasNextWeapon(currentWeapon)) return null;

            return this.weaponsInfoMap[currentIndex + 1];
        }

        public MainCharacterWeaponInfo GetPrevWeapon(MainCharacterWeaponInfo currentWeapon)
        {
            var currentIndex = this.weaponsInfoMap.IndexOf(currentWeapon);

            if (!this.HasPrevWeapon(currentWeapon)) return null;

            return this.weaponsInfoMap[currentIndex - 1];
        }
    }
}

