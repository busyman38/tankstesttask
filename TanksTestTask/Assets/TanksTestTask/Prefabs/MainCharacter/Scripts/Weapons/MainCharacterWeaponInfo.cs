using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TankTestTask.Gameplay
{
    [CreateAssetMenu(fileName = "MainCharacterWeaponInfo", menuName = "TankTestTask/Gameplay/Weapon/New MainCharacterWeaponInfo")]
    public class MainCharacterWeaponInfo : ScriptableObject
    {
        [SerializeField] private string m_id;
        [SerializeField] private bool m_isActiveByDefault;
        [SerializeField] private CharacterSkin m_characterSkin;
        [SerializeField] private int m_damage;
        [SerializeField] private int m_launchForce;
        [SerializeField] private WeaponProjectile m_projectileGO;

        public string id => this.m_id;
        public bool isActiveByDefault => this.m_isActiveByDefault;
        public CharacterSkin characterSkin => this.m_characterSkin;
        public int damage => this.m_damage;
        public int launchForce => this.m_launchForce;
        public WeaponProjectile projectileGO => this.m_projectileGO;
    }
}

