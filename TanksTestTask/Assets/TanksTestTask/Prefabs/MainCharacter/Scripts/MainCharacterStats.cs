using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TankTestTask.Gameplay
{
    [Serializable]
    public class MainCharacterStats : CharacterStats
    {
        public float turnSpeed;
        public float launchProjectileForce;

        public void AddWeaponStats(MainCharacterWeaponInfo weaponInfo)
        {
            this.damage += weaponInfo.damage;
            this.launchProjectileForce += weaponInfo.launchForce;
        }

        public void RemoveWeaponStats(MainCharacterWeaponInfo weaponInfo)
        {
            this.damage -= weaponInfo.damage;
            this.launchProjectileForce -= weaponInfo.launchForce;
        }

        public MainCharacterStats Clone()
        {
            return new MainCharacterStats
            {
                health = this.health,
                armor = this.armor,
                damage = this.damage,
                speed = this.speed,
                turnSpeed = this.turnSpeed,
                launchProjectileForce = this.launchProjectileForce
            };
        }
    }
}
