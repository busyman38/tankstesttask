using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TankTestTask.Gameplay
{
    public class MainCharacter : Character
    {
        [SerializeField] private MainCharacterStats myStats;
        [SerializeField] private CharacterHealthbar uiHealthBar;
        [SerializeField] private Transform m_skinContainer;

        public override CharacterStats currentStats => this.myStats;
        public Transform skinContainer => this.m_skinContainer;
        public MainCharacterWeaponInfo equipedWeapon { get; private set; }

        private MainCharacterStats defaultStats;
        private MainCharacterWeaponInteractor weaponInteractor;
        public CharacterSkin currentSkin { get; private set; }

        private void Start()
        {
            this.SetBehavior<CharacterBehaviourIdle>();
            this.weaponInteractor = Game.GetInteractor<MainCharacterWeaponInteractor>();
            this.defaultStats = this.myStats.Clone();
            this.EquipDefaultWeapon();
            this.uiHealthBar.SetValue(this.currentStats.health/this.defaultStats.health);
        }

        #region BEHAVIOR

        protected override void InitBehaviors()
        {
            this.behaviorsMap = new Dictionary<Type, CharacterBehaviour>();
            this.AddBehavior<CharacterBehaviourIdle>();
            this.AddBehavior<CharacterBehaviourMoveAndAttack>();
        }

        #endregion

        public override void Attack()
        {
            if (!this.behavior.CanAttack())
                return;

            this.behavior.Attack();
        }

        public override void Die()
        {
            Destroy(this.gameObject);
        }

        public override void Hit(object sender, int damage)
        {
            this.currentStats.health -= (int) Mathf.Round(damage * this.currentStats.armor);
            var value = (float) this.currentStats.health / this.defaultStats.health;
            this.uiHealthBar.SetValue(value);
            
            if(this.currentStats.health <= 0) this.Die();
        }

        public override void Move(object sender, float inputDirection)
        {
            if (!this.behavior.CanMove())
                return;

            this.behavior.Move(sender, inputDirection);
        }

        public override void Move(object sender, float inputDirection, float turnValue)
        {
            if (!this.behavior.CanMove())
                return;

            this.behavior.Move(sender, inputDirection, turnValue);
        }

        #region WEAPON

        public void EquipDefaultWeapon()
        {
            var defaultWeaponInfo = this.weaponInteractor.GetDefaultWeapon();
            this.equipedWeapon = defaultWeaponInfo;
            this.myStats.AddWeaponStats(this.equipedWeapon);

            this.UpdateSkin();
        }

        public void EquipPrevWeapon()
        {
            if (!weaponInteractor.HasPrevWeapon(this.equipedWeapon)) return;

            this.myStats.RemoveWeaponStats(this.equipedWeapon);
            this.equipedWeapon = this.weaponInteractor.GetPrevWeapon(this.equipedWeapon);
            this.myStats.AddWeaponStats(this.equipedWeapon);

            this.UpdateSkin();
        }

        public void EquipNextWeapon()
        {
            if (!weaponInteractor.HasNextWeapon(this.equipedWeapon)) return;

            this.myStats.RemoveWeaponStats(this.equipedWeapon);
            this.equipedWeapon = this.weaponInteractor.GetNextWeapon(this.equipedWeapon);
            this.myStats.AddWeaponStats(this.equipedWeapon);

            this.UpdateSkin();
        }

        #endregion

        private void UpdateSkin()
        {
            if (currentSkin != null)
            {
                Destroy(this.currentSkin.gameObject);
                currentSkin = null;
            }

            if (this.equipedWeapon == null) return;

            this.currentSkin = Instantiate(this.equipedWeapon.characterSkin, this.skinContainer);
        }
    }
}

