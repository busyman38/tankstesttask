using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TankTestTask.Gameplay
{
    public class World : MonoBehaviour
    {
        [SerializeField] private Transform m_playerSpawnPoint;

        public Transform playerSpawnPoint => this.m_playerSpawnPoint;

        #region MONO

        private void Awake()
        {
            if (Game.isInitialized)
            {
                this.RegisterWorldInInteractor();
            }
            else
            {
                Game.OnGameInitializedEvent += OnGameInitialized;
            }
        }

        #endregion

        private void RegisterWorldInInteractor()
        {
            var worldInteractor = Game.GetInteractor<WorldInteractor>();
            worldInteractor.RegisterWorld(this);
        }

        #region CALLBACKS

        private void OnGameInitialized()
        {
            Game.OnGameInitializedEvent -= OnGameInitialized;
            this.RegisterWorldInInteractor();
        }

        #endregion
    }
}
