using System;

namespace TankTestTask.Gameplay
{
    public class WorldInteractor : Interactor
    {
        #region EVENTS

        public event Action OnWorldRegisteredEvent;

        #endregion

        public World world { get; private set; }

        public void RegisterWorld(World worldToRegister)
        {
            this.world = worldToRegister;
            this.OnWorldRegisteredEvent?.Invoke();
        }
    }
}

