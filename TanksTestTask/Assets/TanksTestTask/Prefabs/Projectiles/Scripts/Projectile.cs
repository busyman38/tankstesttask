﻿using System;
using TankTestTask.Gameplay;
using UnityEngine;

namespace TanksTestTask.Gameplay
{
    public class Projectile : MonoBehaviour
    {
        [SerializeField] private float m_lifeTime;
        [SerializeField] private Rigidbody m_rb;
        [SerializeField] private LayerMask m_damageMask;
        
        private int m_projectileDamage;
        
        public float lifeTime => this.m_lifeTime;
        public Rigidbody rb => this.m_rb;
        public int projectileDamage => this.m_projectileDamage;
        public LayerMask damageMask => this.m_damageMask;
        
        public virtual void Setup(int damage)
        {
            this.m_projectileDamage = damage;
        }

        private void Awake()
        {
            Destroy(this.gameObject, this.lifeTime);
        }

        private void OnTriggerEnter(Collider other)
        {
            if(1 << other.gameObject.layer != damageMask.value) return;

            var character = other.gameObject.GetComponentInParent<Character>();

            if (!character) return;
            
            character.Hit(this, this.projectileDamage);
            Destroy(this.gameObject);
        }
    }
}