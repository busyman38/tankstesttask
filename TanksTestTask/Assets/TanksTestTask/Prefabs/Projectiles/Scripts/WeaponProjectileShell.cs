﻿using System;
using TankTestTask.Gameplay;
using UnityEngine;

namespace TanksTestTask.Gameplay
{
    public class WeaponProjectileShell : WeaponProjectile
    {
        [SerializeField] private float m_explosionRadius;
        [SerializeField] private float m_explosionForce;
        [SerializeField] private ParticleSystem m_ExplosionParticles;

        public float explosionRadius => this.m_explosionRadius;
        public float explosionForce => this.m_explosionForce;
        
        #region MONO

        private void OnEnable()
        {
            Destroy(this.gameObject, this.lifeTime);
        }

        #endregion
        
        private void OnTriggerEnter(Collider other)
        {
            var colliders = Physics.OverlapSphere(this.transform.position, this.explosionRadius, this.damageMask);

            foreach (var collider in colliders)
            {
                var character = collider.GetComponentInParent<Character>();
                
                if(!character) continue;

                character.rb.AddExplosionForce(this.m_explosionForce, this.transform.position, m_explosionForce);
                character.Hit(this, this.projectileDamage);
            }
            
            m_ExplosionParticles.transform.parent = null;
            m_ExplosionParticles.Play();
            
            var mainModule = m_ExplosionParticles.main;
            Destroy(m_ExplosionParticles.gameObject, mainModule.duration);
            Destroy(gameObject);
        }
    }
}