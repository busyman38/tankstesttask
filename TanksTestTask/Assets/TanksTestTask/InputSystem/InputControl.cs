// GENERATED AUTOMATICALLY FROM 'Assets/TanksTestTask/InputSystem/InputControl.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

namespace TankTestTask.Gameplay
{
    public class @InputControl : IInputActionCollection, IDisposable
    {
        public InputActionAsset asset { get; }
        public @InputControl()
        {
            asset = InputActionAsset.FromJson(@"{
    ""name"": ""InputControl"",
    ""maps"": [
        {
            ""name"": ""Gameplay"",
            ""id"": ""05fb9aea-f0d6-43be-b5f2-f39a8d817a28"",
            ""actions"": [
                {
                    ""name"": ""Move"",
                    ""type"": ""Value"",
                    ""id"": ""559de27a-493f-4f9d-b4f8-f5adda905f3e"",
                    ""expectedControlType"": ""Axis"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Turn"",
                    ""type"": ""Value"",
                    ""id"": ""7fecf339-8fed-49ab-801f-b4b2e9b4d822"",
                    ""expectedControlType"": ""Axis"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Attack"",
                    ""type"": ""Button"",
                    ""id"": ""17fee23f-3703-4969-812f-6dbfbb73a299"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""ChangeWeaponNext"",
                    ""type"": ""Button"",
                    ""id"": ""1cf4c6a5-9650-424f-b509-060e32e2c29a"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""ChangeWeaponPrev"",
                    ""type"": ""Button"",
                    ""id"": ""b27aac3b-92ae-4503-bf22-2ed68e6dcc7d"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": ""Arrows"",
                    ""id"": ""4d7e31d5-d0f2-4f0a-847b-2a999cf06a31"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""Negative"",
                    ""id"": ""c286f600-5d78-4fa9-8c1f-93147059c39c"",
                    ""path"": ""<Keyboard>/downArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard&Mouse"",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""Positive"",
                    ""id"": ""7aa7bc07-0c8d-40a5-83df-8a31fb1ed6c5"",
                    ""path"": ""<Keyboard>/upArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard&Mouse"",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""Arrows"",
                    ""id"": ""89b48169-28e3-466d-9a5f-5ee6e03619c6"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Turn"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""Negative"",
                    ""id"": ""97940e2a-a194-4915-be0f-94355e6b1010"",
                    ""path"": ""<Keyboard>/leftArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard&Mouse"",
                    ""action"": ""Turn"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""Positive"",
                    ""id"": ""791d9fad-2689-4afd-a935-ea6e88b93505"",
                    ""path"": ""<Keyboard>/rightArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard&Mouse"",
                    ""action"": ""Turn"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""3643aaeb-c2ab-4938-a355-b53f7eb71a24"",
                    ""path"": ""<Keyboard>/x"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard&Mouse"",
                    ""action"": ""Attack"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""cc84ed15-958d-4fc3-9806-a7e5b0f23c5c"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard&Mouse"",
                    ""action"": ""ChangeWeaponNext"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""d1ad854a-50ee-4ac7-9e0e-d514f7411dbd"",
                    ""path"": ""<Keyboard>/q"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard&Mouse"",
                    ""action"": ""ChangeWeaponPrev"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": [
        {
            ""name"": ""Keyboard&Mouse"",
            ""bindingGroup"": ""Keyboard&Mouse"",
            ""devices"": [
                {
                    ""devicePath"": ""<Keyboard>"",
                    ""isOptional"": false,
                    ""isOR"": false
                },
                {
                    ""devicePath"": ""<Mouse>"",
                    ""isOptional"": false,
                    ""isOR"": false
                }
            ]
        }
    ]
}");
            // Gameplay
            m_Gameplay = asset.FindActionMap("Gameplay", throwIfNotFound: true);
            m_Gameplay_Move = m_Gameplay.FindAction("Move", throwIfNotFound: true);
            m_Gameplay_Turn = m_Gameplay.FindAction("Turn", throwIfNotFound: true);
            m_Gameplay_Attack = m_Gameplay.FindAction("Attack", throwIfNotFound: true);
            m_Gameplay_ChangeWeaponNext = m_Gameplay.FindAction("ChangeWeaponNext", throwIfNotFound: true);
            m_Gameplay_ChangeWeaponPrev = m_Gameplay.FindAction("ChangeWeaponPrev", throwIfNotFound: true);
        }

        public void Dispose()
        {
            UnityEngine.Object.Destroy(asset);
        }

        public InputBinding? bindingMask
        {
            get => asset.bindingMask;
            set => asset.bindingMask = value;
        }

        public ReadOnlyArray<InputDevice>? devices
        {
            get => asset.devices;
            set => asset.devices = value;
        }

        public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

        public bool Contains(InputAction action)
        {
            return asset.Contains(action);
        }

        public IEnumerator<InputAction> GetEnumerator()
        {
            return asset.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Enable()
        {
            asset.Enable();
        }

        public void Disable()
        {
            asset.Disable();
        }

        // Gameplay
        private readonly InputActionMap m_Gameplay;
        private IGameplayActions m_GameplayActionsCallbackInterface;
        private readonly InputAction m_Gameplay_Move;
        private readonly InputAction m_Gameplay_Turn;
        private readonly InputAction m_Gameplay_Attack;
        private readonly InputAction m_Gameplay_ChangeWeaponNext;
        private readonly InputAction m_Gameplay_ChangeWeaponPrev;
        public struct GameplayActions
        {
            private @InputControl m_Wrapper;
            public GameplayActions(@InputControl wrapper) { m_Wrapper = wrapper; }
            public InputAction @Move => m_Wrapper.m_Gameplay_Move;
            public InputAction @Turn => m_Wrapper.m_Gameplay_Turn;
            public InputAction @Attack => m_Wrapper.m_Gameplay_Attack;
            public InputAction @ChangeWeaponNext => m_Wrapper.m_Gameplay_ChangeWeaponNext;
            public InputAction @ChangeWeaponPrev => m_Wrapper.m_Gameplay_ChangeWeaponPrev;
            public InputActionMap Get() { return m_Wrapper.m_Gameplay; }
            public void Enable() { Get().Enable(); }
            public void Disable() { Get().Disable(); }
            public bool enabled => Get().enabled;
            public static implicit operator InputActionMap(GameplayActions set) { return set.Get(); }
            public void SetCallbacks(IGameplayActions instance)
            {
                if (m_Wrapper.m_GameplayActionsCallbackInterface != null)
                {
                    @Move.started -= m_Wrapper.m_GameplayActionsCallbackInterface.OnMove;
                    @Move.performed -= m_Wrapper.m_GameplayActionsCallbackInterface.OnMove;
                    @Move.canceled -= m_Wrapper.m_GameplayActionsCallbackInterface.OnMove;
                    @Turn.started -= m_Wrapper.m_GameplayActionsCallbackInterface.OnTurn;
                    @Turn.performed -= m_Wrapper.m_GameplayActionsCallbackInterface.OnTurn;
                    @Turn.canceled -= m_Wrapper.m_GameplayActionsCallbackInterface.OnTurn;
                    @Attack.started -= m_Wrapper.m_GameplayActionsCallbackInterface.OnAttack;
                    @Attack.performed -= m_Wrapper.m_GameplayActionsCallbackInterface.OnAttack;
                    @Attack.canceled -= m_Wrapper.m_GameplayActionsCallbackInterface.OnAttack;
                    @ChangeWeaponNext.started -= m_Wrapper.m_GameplayActionsCallbackInterface.OnChangeWeaponNext;
                    @ChangeWeaponNext.performed -= m_Wrapper.m_GameplayActionsCallbackInterface.OnChangeWeaponNext;
                    @ChangeWeaponNext.canceled -= m_Wrapper.m_GameplayActionsCallbackInterface.OnChangeWeaponNext;
                    @ChangeWeaponPrev.started -= m_Wrapper.m_GameplayActionsCallbackInterface.OnChangeWeaponPrev;
                    @ChangeWeaponPrev.performed -= m_Wrapper.m_GameplayActionsCallbackInterface.OnChangeWeaponPrev;
                    @ChangeWeaponPrev.canceled -= m_Wrapper.m_GameplayActionsCallbackInterface.OnChangeWeaponPrev;
                }
                m_Wrapper.m_GameplayActionsCallbackInterface = instance;
                if (instance != null)
                {
                    @Move.started += instance.OnMove;
                    @Move.performed += instance.OnMove;
                    @Move.canceled += instance.OnMove;
                    @Turn.started += instance.OnTurn;
                    @Turn.performed += instance.OnTurn;
                    @Turn.canceled += instance.OnTurn;
                    @Attack.started += instance.OnAttack;
                    @Attack.performed += instance.OnAttack;
                    @Attack.canceled += instance.OnAttack;
                    @ChangeWeaponNext.started += instance.OnChangeWeaponNext;
                    @ChangeWeaponNext.performed += instance.OnChangeWeaponNext;
                    @ChangeWeaponNext.canceled += instance.OnChangeWeaponNext;
                    @ChangeWeaponPrev.started += instance.OnChangeWeaponPrev;
                    @ChangeWeaponPrev.performed += instance.OnChangeWeaponPrev;
                    @ChangeWeaponPrev.canceled += instance.OnChangeWeaponPrev;
                }
            }
        }
        public GameplayActions @Gameplay => new GameplayActions(this);
        private int m_KeyboardMouseSchemeIndex = -1;
        public InputControlScheme KeyboardMouseScheme
        {
            get
            {
                if (m_KeyboardMouseSchemeIndex == -1) m_KeyboardMouseSchemeIndex = asset.FindControlSchemeIndex("Keyboard&Mouse");
                return asset.controlSchemes[m_KeyboardMouseSchemeIndex];
            }
        }
        public interface IGameplayActions
        {
            void OnMove(InputAction.CallbackContext context);
            void OnTurn(InputAction.CallbackContext context);
            void OnAttack(InputAction.CallbackContext context);
            void OnChangeWeaponNext(InputAction.CallbackContext context);
            void OnChangeWeaponPrev(InputAction.CallbackContext context);
        }
    }
}
