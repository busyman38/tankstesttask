using System;
using UnityEngine;
using UnityEngine.InputSystem;

namespace TankTestTask.Gameplay
{
    public class InputManager : MonoBehaviour
    {
		public enum InputDevice
		{
			KeyboardAndMouse
		}

		#region EVENTS

		public event Action<object, bool> OnAllInputLockedEvent;
		public event Action<object, bool> OnGameplayInputLockedEvent;
		public event Action<object, bool> OnUIInputLockedEvent;
		public event Action<InputDevice> OnInputDeviceChangedEvent;

		#endregion

		public static InputManager instance
		{
			get
			{
				if (m_instance == null)
				{
					var go = new GameObject("[INPUT]");
					m_instance = go.AddComponent<InputManager>();
					DontDestroyOnLoad(go);
				}
				return m_instance;
			}
		}

		public static bool isInitialized => m_instance != null;

		private static InputManager m_instance;

		public bool isLoggingEnabled;

		public bool isLocked { get; private set; }
		public bool isGameplayInputLocked { get; private set; }
		public bool isUIInputLocked { get; private set; }
		public InputControl inputGameplay { get; private set; }
		public InputDevice inputDevice { get; private set; }


		#region MONO

		private void Awake()
		{
			this.inputGameplay = new InputControl();
			this.inputGameplay.Enable();
		}

		private void Update()
		{
			if (Keyboard.current.anyKey.wasPressedThisFrame)
				this.SetDevice(InputDevice.KeyboardAndMouse);

			if (Mouse.current.wasUpdatedThisFrame)
				this.SetDevice(InputDevice.KeyboardAndMouse);
		}

		#endregion

		private void SetDevice(InputDevice device)
		{
			this.inputDevice = device;
			this.OnInputDeviceChangedEvent?.Invoke(device);
		}

		public void LockAllInput(object sender)
		{
			this.LockGameplayInput(sender);
			this.OnAllInputLockedEvent?.Invoke(sender, this.isLocked);
			this.Log($"INPUT: Locked ALL by {sender.GetType()}");
		}

		public void UnlockAllInput(object sender)
		{
			this.UnlockGameplayInput(sender);
			this.OnAllInputLockedEvent?.Invoke(sender, this.isLocked);
			this.Log($"INPUT: Unlocked ALL by {sender.GetType()}");
		}

		public void LockGameplayInput(object sender)
		{
			this.inputGameplay.Disable();
			this.isGameplayInputLocked = true;
			this.OnGameplayInputLockedEvent?.Invoke(sender, this.isGameplayInputLocked);
			this.Log($"INPUT: Locked GAMPLAY by {sender.GetType()}");
		}

		public void UnlockGameplayInput(object sender)
		{
			this.inputGameplay.Enable();
			this.isGameplayInputLocked = false;
			this.OnGameplayInputLockedEvent?.Invoke(sender, this.isGameplayInputLocked);
			this.Log($"INPUT: Locked GAMPLAY by {sender.GetType()}");
		}

		private void Log(string text)
		{
			if (this.isLoggingEnabled)
				Debug.Log(text);
		}

	}
}
